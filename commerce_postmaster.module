<?php

/**
 * @file
 * This is the main module file.
 *
 * Contains all of the hook functions required
 * for the module.
 */

/**
 * Implements hook_menu().
 */
function commerce_postmaster_menu() {
  $items = array();

  // Main config page.
  $items['admin/commerce/config/postmaster'] = array(
    'title' => 'Commerce Postmaster Settings',
    'description' => 'Configuration settings for Commerce Postmaster',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_postmaster_admin_form'),
    'access arguments' => array('administer_postmaster'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'commerce_postmaster.admin.inc',
    'weight' => 20,
  );

  $items['admin/commerce/config/postmaster/settings'] = array(
    'title' => 'Commerce Postmaster Settings',
    'description' => 'Configuration settings for Commerce Postmaster',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_postmaster_admin_form'),
    'access arguments' => array('administer_postmaster'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -19,
    'file' => 'commerce_postmaster.admin.inc',
  );

  // Set the default options for shipping and provide extra fields on products
  // and orders if required.
  $items['admin/commerce/config/postmaster/ship_options'] = array(
    'title' => 'Shipping Options',
    'description' => 'Shipping options for internal shipping rates. This is used when creating tracking ID\'s through the postmaster API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_commerce_postmaster_shipping_options_form'),
    'access arguments' => array('administer_postmaster'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'commerce_postmaster.admin.inc',
    'weight' => -18,
  );

  // A test form to validate an address.
  $items['admin/commerce/config/postmaster/validate_address'] = array(
    'title' => 'Validate Address',
    'description' => 'Validate an address',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_commerce_postmaster_validate_address_form'),
    'access arguments' => array('create_postmaster_shipping'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'commerce_postmaster.admin.inc',
    'weight' => -17,
  );

  // A test form to create a shipment.
  $items['admin/commerce/config/postmaster/create_shipment'] = array(
    'title' => 'Create Shipment',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_commerce_postmaster_create_shipment_form'),
    'access arguments' => array('create_postmaster_shipping'),
    'file' => 'commerce_postmaster.admin.inc',
    'weight' => -16,
  );

  // A test form to check tracking status.
  $items['admin/commerce/config/postmaster/check_tracking'] = array(
    'title' => 'Check Tracking',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_commerce_postmaster_check_tracking_form'),
    'access arguments' => array('check_postmaster_tracking'),
    'file' => 'commerce_postmaster.admin.inc',
    'weight' => -15,
  );

  return $items;
}

/**
 * Implements hook_permission().
 *
 * Setup the permissions required for this module.
 */
function commerce_postmaster_permission() {
  return array(
    'administer_postmaster' => array(
      'title' => t('Administer Postmaster API Settings'),
    ),
    'create_postmaster_shipping' => array(
      'title' => t('Create Postmaster Shipping'),
    ),
    'manage shipping options' => array(
      'title' => t('Manage Postmaster Shipping Options'),
    ),
    'check_postmaster_tracking' => array(
      'title' => t('Check Postmaster with Tracking key'),
    ),
  );
}

/**
 * Function commerce_postmaster_admin_form.
 *
 * Admin settings formcommerce_postmaster_admin_form.
 *
 * @return array
 *   System settings form.
 */
function commerce_postmaster_admin_form() {
  $form = array();

  $form['commerce_postmaster_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Postmaster API Key'),
    '#default_value' => variable_get('commerce_postmaster_api_key', 'Need to set API Key'),
    '#size' => 50,
    '#maxlength' => 70,
    '#description' => t('Your api key from your !dashboard.', array('!dashboard' => l(t('Postmaster API Dashboard'), 'https://www.postmaster.io/dashboard/settings'))),
    '#required' => TRUE,
  );
  $form['commerce_postmaster_country'] = array(
    '#type' => 'select',
    '#title' => t('Country of Origin'),
    '#options' => CommercePostmaster::countryGetList(),
    '#required' => TRUE,
    '#description' => t('Please enter the country where your parcels will ship from. This should match the from address settings in your postmaster account.'),
    '#default_value' => variable_get('commerce_postmaster_country', t('US')),
  );
  $form['commerce_postmaster_delete_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete Fields on Uninstall'),
    '#description' => t('If you check this box the commerce postmaster fields will be deleted when the module is uninstalled. This means you will leave any data contained within these fields.'),
    '#default_value' => variable_get('commerce_postmaster_delete_fields', FALSE),
  );
  return system_settings_form($form);
}

/**
 * Function _commerce_postmaster_shipping_options_form.
 *
 * Shipping options for internal shipping rates. This is used when creating
 * tracking ID's through the postmaster API
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   A form array.
 */
function _commerce_postmaster_shipping_options_form(array $form, array &$form_state) {
  return CommercePostmasterAdmin::shippingOptionsForm($form, $form_state);
}

/**
 * Function _commerce_postmaster_shipping_options_form_validate.
 *
 * Validation for the shipping options form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function _commerce_postmaster_shipping_options_form_validate(array $form, array &$form_state) {
  CommercePostmasterAdmin::shippingOptionsFormValidate($form, $form_state);
}

/**
 * Function _commerce_postmaster_shipping_options_form_submit.
 *
 * Submit function for the shipping options form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function _commerce_postmaster_shipping_options_form_submit(array $form, array &$form_state) {
  CommercePostmasterAdmin::shippingOptionsFormSubmit($form, $form_state);
}

/**
 * Function _commerce_postmaster_check_tracking_form.
 *
 * Create the test tracking form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   A form array.
 */
function _commerce_postmaster_check_tracking_form(array $form, array &$form_state) {
  return CommercePostmasterAdmin::checkTrackingForm();
}

/**
 * Function _commerce_postmaster_check_tracking_form_validate.
 *
 * Validation for the test tracking form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function _commerce_postmaster_check_tracking_form_validate(array &$form, array &$form_state) {
  CommercePostmasterAdmin::checkTrackingFormValidate($form, $form_state);
}

/**
 * Function _commerce_postmaster_check_tracking_form_submit.
 *
 * Submit function for the test tracking form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function _commerce_postmaster_check_tracking_form_submit(array $form, array $form_state) {
  CommercePostmasterAdmin::checkTrackingFormSubmit($form, $form_state);
}

/**
 * Function _commerce_postmaster_validate_address_form.
 *
 * Test address validation form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   A form array.
 */
function _commerce_postmaster_validate_address_form(array $form, array &$form_state) {
  return CommercePostmasterAdmin::validateAddressForm();
}

/**
 * Function _commerce_postmaster_validate_address_form_submit.
 *
 * Validation for the test validation form
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function _commerce_postmaster_validate_address_form_submit(array $form, array &$form_state) {
  CommercePostmasterAdmin::validateAddressFormSubmit($form, $form_state);
}

/**
 * Function _commerce_postmaster_create_shipment_form.
 *
 * Test form to create shipments.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   A form array.
 */
function _commerce_postmaster_create_shipment_form(array $form, array &$form_state) {
  return CommercePostmasterAdmin::shippingForm();
}

/**
 * Function _commerce_postmaster_create_shipment_form_submit.
 *
 * Submit function for the test shipment form
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function _commerce_postmaster_create_shipment_form_submit(array $form, array &$form_state) {
  CommercePostmasterAdmin::shippingFormSubmit($form, $form_state);
}

/**
 * Function commerce_postmaster_create_tracking_wrapper.
 *
 * This function calls the CommercePostmaster class
 * commerce_postmaster_create_tracking function to get a tracking id, then
 * assigns this id to the order.
 */
function commerce_postmaster_create_tracking_wrapper(array $form) {
  // Get the order entity and pass it to the function.
  $order = $form['#entity'];
  $track_id = CommercePostmaster::createTracking($order);
  // If it was successful, $track_id will contain the returned id from
  // postmaster.
  if ($track_id) {
    // Save it to the current order.
    $order->field_cm_tracking_id[LANGUAGE_NONE][0]['value'] = $track_id;
    $order->status = 'track_id_created';
    field_attach_update('commerce_order', $order);
    commerce_order_save($order);
  }
}

/**
 * Function commerce_postmaster_check_tracking_wrapper.
 *
 * This calls the CommercePostmaster class check_tracking to check the tracking
 * id and pass the message back to the user.
 */
function commerce_postmaster_check_tracking_wrapper(array $form) {
  // Get the order entity and pass it to the function.
  $papi_id = $form['#entity']->field_cm_tracking_id[LANGUAGE_NONE][0]['value'];
  $status = CommercePostmaster::checkTracking($papi_id);
  // If it was successful, display the status.
  if ($status) {
    $message = '<table style="width:40%"><tr><td width="20%">Status:</td><td>' .
      $status['status'] . '</td></tr>';
    $message = '<tr><td colspan="2"><b>History</b></td></tr>';
    foreach ($status['history'] as $item) {
      $message = '<tr><td>Status:</td><td>' . $item['status'] . '</td></tr>';
      $message = '<tr><td>Time:</td><td>' . format_date($item['timestamp']) . '</td></tr>';
      $message = '<tr><td>Description:</td><td>' . $item['description'] . '</td></tr>';
    }
    $message .= '<tr><td>Last Update:</td><td>' . format_date($status['last_update']) . '</td></tr></table>';
    $message = filter_xss($message, array('td', 'tr', 'table', 'b'));
    drupal_set_message($message);

  }
}

/**
 * Function commerce_postmaster_libraries_info.
 *
 * Provide the required information to the Libraries API for the Postmaster
 * Library files.
 *
 * @return array
 *   Libraries array.
 */
function commerce_postmaster_libraries_info() {
  $libraries['postmaster'] = array(
    'name' => 'Postmaster API',
    'vendor url' => 'https://www.postmaster.io/',
    'download url' => 'https://github.com/postmaster/postmaster-php',
    'path' => 'lib',
    'version callback' => '_commerce_postmaster_return_version',
    'version arguments' => array(
      'file' => 'composer.json',
    ),
    'files' => array(
      'php' => array('Postmaster.php'),
    ),
  );
  return $libraries;
}

/**
 * Funciton _commerce_postmaster_return_version.
 *
 * Return the current version of the Postmaster API for the Libraries API.
 *
 * @param array $libraries
 *   Libraries array.
 * @param array $data
 *   Array of data associated with the libraries version call.
 *
 * @return string
 *   the current version of the API
 */
function _commerce_postmaster_return_version(array $libraries, array $data) {
  $jsonfile = file_get_contents(libraries_get_path('postmaster') . '/' . $data['file']);
  $versioninfo = drupal_json_decode($jsonfile);
  return $versioninfo['version'];
}

/**
 * Implements hook_form_alter() for form_commerce_product_ui_product().
 *
 * Alter the commerce products form to move the shipping options to a vertical
 * tab.
 */
function commerce_postmaster_form_commerce_product_ui_product_form_alter(array &$form, array &$form_state, $form_id) {
  // If any of the CM fields exist.
  if (isset($form['commerce_pm_service_level']) || isset($form['commerce_pm_intl_service_level']) || isset($form['commerce_pm_packaging']) || isset($form['commerce_pm_label_formats']) || isset($form['commerce_pm_label_sizes'])) {
    $form['cm_shipopt'] = array(
      '#type' => 'fieldset',
      '#title' => t('Shipping Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#weight' => 40,
    );
    // Copy the field to the vertical tab then unset the original field to move.
    if (isset($form['commerce_pm_service_level'])) {
      $form['cm_shipopt']['commerce_pm_service_level'] = $form['commerce_pm_service_level'];
      unset($form['commerce_pm_service_level']);
    }
    if (isset($form['commerce_pm_intl_service_level'])) {
      $form['cm_shipopt']['commerce_pm_intl_service_level'] = $form['commerce_pm_intl_service_level'];
      unset($form['commerce_pm_intl_service_level']);
    }
    if (isset($form['commerce_pm_packaging'])) {
      $form['cm_shipopt']['commerce_pm_packaging'] = $form['commerce_pm_packaging'];
      unset($form['commerce_pm_packaging']);
    }
    if (isset($form['commerce_pm_label_formats'])) {
      $form['cm_shipopt']['commerce_pm_label_formats'] = $form['commerce_pm_label_formats'];
      unset($form['commerce_pm_label_formats']);
    }
    if (isset($form['commerce_pm_label_sizes'])) {
      $form['cm_shipopt']['commerce_pm_label_sizes'] = $form['commerce_pm_label_sizes'];
      unset($form['commerce_pm_label_sizes']);
    }
  }
}

/**
 * Function commerce_postmaster_form_commerce_order_ui_order_form_alter.
 *
 * Implements hook_form_alter.
 * This function hides the shipping fields if the current user does
 * not have permissions to set them. It also sets the default
 * values for the fields that are present.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 * @param string $form_id
 *   The id of the form.
 */
function commerce_postmaster_form_commerce_order_ui_order_form_alter(array &$form, array &$form_state, $form_id) {
  if (variable_get('commerce_postmaster_order_sl', 0) || variable_get('commerce_postmaster_order_sli', 0) || variable_get('commerce_postmaster_order_pak', 0) || variable_get('commerce_postmaster_order_lf', 0) || variable_get('commerce_postmaster_order_ls', 0)) {
    // If the current user has access, create the vertical tab.
    if (user_access('manage shipping options')) {
      $form['cm_shipopt'] = array(
        '#type' => 'fieldset',
        '#title' => t('Shipping Options'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#group' => 'additional_settings',
        '#weight' => 40,
      );
    }
  }
  if (isset($form['commerce_pm_service_level'])) {
    // Hide the item if the current user doesn't have access.
    $form['commerce_pm_service_level']['#access'] = user_access('manage shipping options');
    // Get the default value from the class.
    $form['commerce_pm_service_level'][LANGUAGE_NONE]['#default_value'][0] = CommercePostmaster::orderSlDefault($form['#entity']);
    // Move the field to the vertical tab.
    $form['cm_shipopt']['commerce_pm_service_level'] = $form['commerce_pm_service_level'];
    unset($form['commerce_pm_service_level']);
  }
  if (isset($form['commerce_pm_intl_service_level'])) {
    // Hide the item if the current user doesn't have access.
    $form['commerce_pm_intl_service_level']['#access'] = user_access('manage shipping options');
    // Get the default value from the class.
    $form['commerce_pm_intl_service_level'][LANGUAGE_NONE]['#default_value'][0] = CommercePostmaster::orderSliDefault($form['#entity']);
    // Move the field to the vertical tab.
    $form['cm_shipopt']['commerce_pm_intl_service_level'] = $form['commerce_pm_intl_service_level'];
    unset($form['commerce_pm_intl_service_level']);
  }
  if (isset($form['commerce_pm_packaging'])) {
    // Hide the item if the current user doesn't have access.
    $form['commerce_pm_packaging']['#access'] = user_access('manage shipping options');
    // Get the default value from the class.
    $form['commerce_pm_packaging'][LANGUAGE_NONE]['#default_value'][0] = CommercePostmaster::orderPakDefault($form['#entity']);
    // Move the field to the vertical tab.
    $form['cm_shipopt']['commerce_pm_packaging'] = $form['commerce_pm_packaging'];
    unset($form['commerce_pm_packaging']);
  }
  if (isset($form['commerce_pm_label_formats'])) {
    // Hide the item if the current user doesn't have access.
    $form['commerce_pm_label_formats']['#access'] = user_access('manage shipping options');
    // Get the default value from the class.
    $form['commerce_pm_label_formats'][LANGUAGE_NONE]['#default_value'][0] = CommercePostmaster::orderLfDefault($form['#entity']);
    // Move the field to the vertical tab.
    $form['cm_shipopt']['commerce_pm_label_formats'] = $form['commerce_pm_label_formats'];
    unset($form['commerce_pm_label_formats']);
  }
  if (isset($form['commerce_pm_label_sizes'])) {
    // Hide the item if the current user doesn't have access.
    $form['commerce_pm_label_sizes']['#access'] = user_access('manage shipping options');
    // Get the default value from the class.
    $form['commerce_pm_label_sizes'][LANGUAGE_NONE]['#default_value'][0] = CommercePostmaster::orderLsDefault($form['#entity']);
    // Move the field to the vertical tab.
    $form['cm_shipopt']['commerce_pm_label_sizes'] = $form['commerce_pm_label_sizes'];
    unset($form['commerce_pm_label_sizes']);
  }

  $hidecustoms = CommercePostmaster::shippingInternational($form['#entity']);
  if ($hidecustoms) {
    unset($form['field_cm_type']);
    unset($form['field_cm_comments']);
  }
  // Only show the create button if the status is pending, charged or
  // processing.
  $show_create = $form['#entity']->status == 'pending' || $form['#entity']->status == 'cardonfile_charged' || $form['#entity']->status == 'processing';
  $show_check = FALSE;
  if (isset($form_state['commerce_order']->field_cm_tracking_id[LANGUAGE_NONE]) &&
    isset($form_state['commerce_order']->field_cm_tracking_id[LANGUAGE_NONE][0])) {
    $show_check = $form_state['commerce_order']->field_cm_tracking_id[LANGUAGE_NONE][0]['value'] != '';
  }
  if ($show_create && !$show_check) {
    $form['actions']['submit1'] = array(
      '#type' => 'submit',
      '#value' => t('Create Tracking ID'),
      '#weight' => 40,
      '#access' => user_access('edit any commerce_order entity of bundle commerce_order'),
      '#submit' => array(
        0 => 'commerce_postmaster_create_tracking_wrapper',
      ),
    );
  }
  // Only show the check tracking button if there's a tracking id.
  if ($show_check) {
    $form['actions']['submit2'] = array(
      '#type' => 'submit',
      '#value' => t('Checking Shipping Status'),
      '#weight' => 40,
      '#access' => user_access('edit any commerce_order entity of bundle commerce_order'),
      '#submit' => array(
        0 => 'commerce_postmaster_check_tracking_wrapper',
      ),
    );
  }
}

/**
 * Implements hook_help().
 *
 * Function commerce_postmaster_help
 * Display the Readme file in the drupal help pages.
 */
function commerce_postmaster_help($path, $arg) {
  switch ($path) {
    case 'admin/help#commerce_postmaster':
      $output = file_get_contents(drupal_get_path('module', 'commerce_postmaster') . '/README.txt');
      return module_exists('markdown') ? filter_xss_admin(module_invoke('markdown', 'filter', 'process', 0, -1, $output)) : '<pre>' . check_plain($output) . '</pre>';
  }
}
