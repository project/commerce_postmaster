
Commerce Postmaster for Drupal 7.x
----------------------------
Commerce Postmaster provides integration with the postmaster API,
http://postmaster.io; this enables users to create parcels for FedEx, USPS and
UPS.

Installation
------------
Please note, these instructions to not contain full instructions on how to
setup postmaster, please refer tot he postmaster documentation to complete this
setup.

Commerce Postmaster can be installed like any other Drupal module -- place it
in the modules directory for your site and enable it on the
'admin/build/modules' page. Ensure you also have the dependent modules
installed, libraries, commerce and commerce_custom_order_status

You will also need to download the postmaster php api from GitHub,
https://github.com/postmaster/postmaster-php and unpack it in the
sites/all/libraries folder. Rename the unpacked folder to postmaster.

Get a postmaster account, go to https://www.postmaster.io/signup and choose a
method to signup. In the top right corner, click on your username, then in the
dropdown menu select Account Settings. Select the carriers you would like to
use. If you want to use UPS or FedEx, follow the instructions on this screen to
setup your account in postmaster.

Click on the API Keys/Data tab. Here you can copy your test and production API
keys. Go to /admin/commerce/config/postmaster, enter your API key and click
save. To switch between developer and production mode, change this setting to
the appropriate API key from postmaster.


Using Commerce Postmaster
-------------------
Commerce Postmaster enables the site builder to enable default shipping options,
such as the size of the package and the type or label, see the postmaster
documentation for further info https://www.postmaster.io/docs.

You can configure the default options on the shipping options tab
admin/commerce/config/postmaster/ship_options. You can set overall site default
values. You can also select to add shipping options fields to your orders to
allow you to select different shipping options per order. You can also select
to store defaults against individual products. Per product defaults only work
when using the per order fields. When a user makes an order with only one
product and that product has default shipping options, the order shipping
options will default to the products shipping defaults.

When the order status is either pending, cardonfile_charged or processing, the
order form will show a Create Tracking ID button. Clicking this button will
send the order details through to postmaster, create the parcel with the
appropriate carrier and return a Tracking ID, which will be stored in the
Tracking ID field which has been attached to the Order Entity by this module.
The carrier is determined by the machine name of the shipping line item attached
to the current order. The machine name of the shipping item must contain either
fedex, usps or ups, this is case insensitive.

The site builder can use this field in emails to the user, or simply display it
to the customer so they can use it to track their order through the carrier.
In a later version the maintainers would like to add tracking data from the
carrier directly to order to simplify the customer experience.

The module also provides Validate Address, Create shipment and Check Tracking
forms within the settings page to allow the site builder to test the
functionality of the module. Although Check Tracking is provided here, it has
not been fully implemented into this module at present.

Uninstalling Commerce Postmaster
-------------------
There is a "Delete Fields on Uninstall" checkbox on the main module config form
admin/commerce/config/postmaster. If this is checked, when the module is
uninstalled it will also delete all of the fields that come with Commerce
Postmaster. If you check this box all historical data will also be lost of
Commerce Postmaster when you uninstall the module.

If this is unchecked, the fields will remain attached to the commerce entities,
it is then up to you to make the changes you require from there, either
deleting, disabling or leaving.


Maintainers
-----------

- kafmil (Kurt Foster)
