<?php
/**
 * @file
 * Contains CommercePostmasterAdmin.
 *
 * This class provides the functions required to integrate with the Postmaster
 * API Library.
 */

class CommercePostmaster {

  // The array of shipping details.
  public static $shipment = array();

  /**
   * Gets the shipment array or returns an error is one doesn't exist.
   *
   * @return array
   *   the shipment array of shipping details
   */
  public static function getShipment() {
    if (!count(self::$shipment)) {
      return array('error', 'A shipping item has not been created, Commerce Postmaster is unable to proceed.');
    }

    return self::$shipment;
  }

  /**
   * Takes a commerce order and creates the shipping item array.
   *
   * @param object $order
   *   A commerce_order that needs to be shipped.
   */
  public static function setShipment($order) {
    self::$shipment = self::prepareShipment($order);
  }

  /**
   * Validates the order address.
   *
   * Take in address details in an array and validate it with the Postmaster
   * API.
   *
   * @param array $address
   *   An array containing address details.
   *
   * @return array
   *   An array with the validation results.
   */
  public static function validateAddress(array $address) {
    if (($library = libraries_load('postmaster')) && !empty($library['loaded'])) {
      Postmaster::setApiKey(variable_get('commerce_postmaster_api_key', ''));
      try {
        // Ensure there are only two characters for the state.
        $address['state'] = strtoupper(substr($address['state'], 0, 2));
        $valid = Postmaster_AddressValidation::validate($address);
        $return = drupal_json_decode($valid->__toJSON());
      }
      catch (Postmaster_Error $e) {
        $return = FALSE;
        if ($e->getMessage() == 'Invalid authorization header.') {
          drupal_set_message(t(
            'Your API key is invalid. !config to set your API key. Check the !readme for further information.',
            array(
              '!config' => l(t('Click here'), 'admin/commerce/config/postmaster'),
              '!readme' => l(t('read me'), 'admin/help/commerce_postmaster'),
            )
            ),
            'error'
          );
        }
        else {
          drupal_set_message(t('Address validation failed: <p>%message</p><p>', array('%message' => $e->getMessage())), 'error');
          watchdog_exception('commerce_postmaster', $e, 'Postmaster API Error');
        }
      }
      return $return;
    }
    else {
      self::setApiErrorMesg();
    }
    return array();
  }

  /**
   * Creates an address array for validation from a shippment array.
   *
   * Take in a complete shipment info array, take out the required info to
   * validate the address and pass it to the validateAddress function.
   *
   * @param array $shipment_info
   *   The shipment info array.
   *
   * @return string
   *   The status of the validation output.
   */
  protected static function validateFromShipment(array $shipment_info) {
    if (is_null($shipment_info)) {
      $shipment_info = self::$shipment;
    }

    if (($library = libraries_load('postmaster')) && !empty($library['loaded'])) {
      $return = self::validateAddress(array(
          'company' => $shipment_info['to']['company'],
          'contact' => $shipment_info['to']['contact'],
          'line1' => $shipment_info['to']['line1'],
          'line2' => isset($shipment_info['to']['line2']) ? $shipment_info['to']['line2'] : '',
          'line3' => isset($shipment_info['to']['line3']) ? $shipment_info['to']['line2'] : '',
          'city' => $shipment_info['to']['city'],
          'state' => substr($shipment_info['to']['state'], 0, 2),
          'zip_code' => $shipment_info['to']['zip_code'],
          'country' => $shipment_info['to']['country'],
        ));
      return $return['status'];
    }
    else {
      self::setApiErrorMesg();
    }
    return '';
  }

  /**
   * Send a shipment creation request to the Postmster API.
   *
   * Call the Postmaster API to create a new shipment item and return a
   * tracking id.
   *
   * @param array $shipment_info
   *   The shipment array of shipping details.
   *
   * @return array
   *   An array containing the shipping details including the tracking id if
   *   successful.
   */
  public static function createShipment(array $shipment_info = NULL) {
    if (is_null($shipment_info)) {
      $shipment_info = self::$shipment;
    }
    if (($library = libraries_load('postmaster')) && !empty($library['loaded'])) {
      $validate = self::validateFromShipment($shipment_info);
      if ($validate == 'OK') {
        try {
          $return = drupal_json_decode(Postmaster_Shipment::create($shipment_info));
        }
        catch (Postmaster_Error $e) {
          $return = FALSE;
          drupal_set_message(t('Creating a new shipping item via postmaster failed: <p>%message</p><p>', array('%message' => $e->getMessage())), 'error');
          watchdog(WATCHDOG_ERROR, 'Postmaster error, Order: %order, createShipment(): %message. Stacktrace: %trace. HTTP Body: %httpbody. HTTP Status: %httpstatus. JSON Body: %jsonbody (Code: %jsoncode)', array(
              '%message' => $e->getMessage(),
              '%trace' => $e->getTraceAsString(),
              '%httpbody' => $e->http_body,
              '%httpstatus' => $e->http_status,
              '%htjsonbodytpbody' => $e->json_body['message'],
              '%jsoncode' => $e->json_body['code']));
        }
      }
      else {
        watchdog(WATCHDOG_ERROR, 'Postmaster address validation error, Order: %order: %message.', array('%message' => $validate));
        $return = FALSE;
      }
      return $return;
    }
    else {
      self::setApiErrorMesg();
    }
    return array();
  }

  /**
   * Checks the current shipping status passing in the Postmaster Order Id.
   *
   * @param string $papi_id
   *   The tracking id from postmaster.
   *
   * @return array
   *   The current shipping status, including a history of updates.
   */
  public static function checkTracking($papi_id) {
    if (($library = libraries_load('postmaster')) && !empty($library['loaded'])) {
      Postmaster::setApiKey(variable_get('commerce_postmaster_api_key', ''));
      try {
        $return = drupal_json_decode(Postmaster_Tracking::track($papi_id));
      }
      catch (Postmaster_Error $e) {
        $return = FALSE;
        drupal_set_message(t('Checking the shipping item via postmaster failed: <p>%message</p><p>', array('%message' => $e->getMessage())), 'error');
        watchdog_exception('commerce_postmaster', $e, 'Postmaster API Error');
      }
      return $return;
    }
    else {
      self::setApiErrorMesg();
    }
    return array();
  }

  /**
   * Creates a tracking ID by creating a shipment via createShipment().
   *
   * Take in a commerce order and pass the details to the createShipment
   * function. Handle error display to the user. Return the tracking id to the
   * order if successful.
   *
   * @param object $order
   *   A commerce_order.
   *
   * @return string
   *   The created tracking id.
   */
  public static function createTracking($order) {

    $return = FALSE;

    // If the tracking id has not already been set.
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    if (empty($wrapper->field_order_tracking_id->value)) {
      // Create shipment in postmaster.io.
      self::setShipment($order);

      $data = self::createShipment();

      // Check if the tracking id has been created.
      if (isset($data['tracking'][0])) {
        // We have successfully created tracking into postmaster.io so update
        // field accordingly.
        $order->field_order_tracking_id->value = $data['tracking'][0];
        field_attach_update('commerce_order', $order);
        commerce_order_save($order);

        drupal_set_message(t('Shipment successfully created. Tracking ID: @tracking_id', array('@tracking_id' => $data['tracking'][0])), 'status');
        $return = $data['tracking'][0];
      }
      elseif (isset($data['message'])) {
        watchdog('Postmaster', 'order @order_id: <em>@message</em>. Error code: <em>@code</em>.', array(
          '@order_id' => $order->order_id,
          '@message' => $data['message'],
          '@code' => $data['code'],
        ), WATCHDOG_ERROR);
        drupal_set_message(t('Postmaster failed to create a shipment. Message: <em>@message</em>. Error code: <em>@code</em>.', array('@message' => $data['message'], '@code' => $data['code'])), 'error');
      }
      // If it has been set then send a warning.
    }
    else {
      drupal_set_message(t('A tracking ID already exists for this order. Please check the tracking ID to make sure you do not create two shipments. Delete the tracking ID and save the order if this ID is incorrect. Tracking ID: @tracking_id.', array('@tracking_id' => $order->field_order_tracking_id->value)), 'warning');
    }

    return $return;

  }

  /**
   * Prepares the array structure for a shipment as required by postmaster.io.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return array
   *   The shipment array of shipping details.
   */
  protected static function prepareShipment($order) {
    $ship_data['to'] = array();

    if (isset($order->commerce_customer_shipping[LANGUAGE_NONE]) && isset($order->commerce_customer_shipping[LANGUAGE_NONE][0])) {
      $shipping_profile_id = $order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id'];

      $shipping_profile = commerce_customer_profile_load($shipping_profile_id);

      $address = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0];

      // Add contact name.
      if (isset($address['name_line'])) {
        $ship_data['to']['contact'] = $address['name_line'];
      }
      else {
        $ship_data['to']['contact'] = $address['first_name'] . ' ' . $address['last_name'];
      }

      // Add company.
      if (!empty($address['organisation_name'])) {
        $ship_data['to']['company'] = $address['organisation_name'];
      }
      else {
        $ship_data['to']['company'] = $ship_data['to']['contact'];
      }

      // Add line1.
      if (!empty($address['thoroughfare'])) {
        $ship_data['to']['line1'] = $address['thoroughfare'];
      }

      // Add line2.
      if (!empty($address['premise'])) {
        $ship_data['to']['line2'] = $address['premise'];
      }

      // Add city.
      if (!empty($address['locality'])) {
        $ship_data['to']['city'] = $address['locality'];
        // $ship_data['to']['city'] = 'Hamburg';
      }

      // Add state.
      if (!empty($address['administrative_area'])) {
        $ship_data['to']['state'] = substr($address['administrative_area'], 0, 2);
      }

      // Add zip_code.
      if (!empty($address['postal_code'])) {
        $ship_data['to']['zip_code'] = $address['postal_code'];
      }

      // Add country.
      if (!empty($address['country'])) {
        $ship_data['to']['country'] = $address['country'];
      }
      if (!empty($shipping_profile->field_cust_profile_phone_number[LANGUAGE_NONE][0]['value'])) {
        $ship_data['to']['phone_no'] = preg_replace("/[^0-9,.]/", "", $shipping_profile->field_cust_profile_phone_number[LANGUAGE_NONE][0]['value']);
      }
      else {
        $ship_data['to']['phone_no'] = '0000000000';
      }

      // Prepare package.
      $package = array();
      $dimensions = self::getDimensions($order);
      $package['width'] = $dimensions['width'];
      $package['length'] = $dimensions['length'];
      $package['height'] = $dimensions['height'];
      $package['weight'] = $dimensions['weight'];

      // If it's international then you have to add customs info.
      if ($address['country'] != 'US') {
        $package['customs']['type'] = isset($order->field_cm_tracking_id[LANGUAGE_NONE]) ? $order->field_cm_tracking_id[LANGUAGE_NONE][0]['value'] : 'Other';
        $package['customs']['comments'] = isset($order->field_cm_comments[LANGUAGE_NONE][0]['value']) ? $order->field_cm_comments[LANGUAGE_NONE][0]['value'] : '';
        $package['customs']['contents'] = $dimensions['customs'];
      }

      $ship_data['package'] = $package;
      $ship_data['carrier'] = self::getCarrier($order);
      $ship_data['service'] = self::getServiceLevel($order, $address['country']);
      $ship_data['po_number'] = $order->order_number;
      $formats = self::getLabelFormats($order);
      $ship_data['packaging'] = self::getPackaging($order);
      $ship_data['label'] = array(
        'type' => self::getLabelTypes($formats),
        'format' => $formats,
        'size' => self::getLabelSizes($order),
      );

      return $ship_data;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns the Postmaster API carrier short code based on an order.
   *
   * Check the shipping line item, it must contain either usps, fedex or ups
   * in the name, case insensitive.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return string
   *   The shipping carrier string required to create a shipment in postmaster.
   */
  protected static function getCarrier($order) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($wrapper->commerce_line_items as $line_item_wrapper) {
      if ($line_item_wrapper->type->value() == 'shipping') {
        $keywords = array('fedex', 'usps', 'ups');
        $shippingvalue = $line_item_wrapper->commerce_shipping_service->value();
        foreach ($keywords as $keyword) {
          if (stristr($shippingvalue, $keyword) !== FALSE) {
            return $keyword;
          }
        }
      }
    }
    return '';
  }

  /**
   * Get the dimensions of the entire package to send in the shipment.
   *
   * Collect the dimensions from the order, if these are blank then get them
   * from the connected products, finally fall back to the defaults.
   *
   * @param object $order
   *   A commerce order entity.
   *
   * @return array
   *   An array of dimensions and customs items.
   */
  protected static function getDimensions($order) {
    $default = TRUE;
    $weight = 0;
    $height = 0;
    $length = 0;
    $width = 0;
    $customs = array();

    // Get a list of all of the product types.
    $product_types = array();
    foreach (commerce_product_types() as $commerce_product_type) {
      $product_types[] = $commerce_product_type['type'];
    }

    // Loop through each product, add the dimensions and create customs arrays.
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($wrapper->commerce_line_items as $line_item_wrapper) {
      // If the line item is a product reference.
      if (in_array($line_item_wrapper->type->value(), $product_types)) {
        if (isset($line_item_wrapper->commerce_product->commerce_pm_weight)) {
          $default = FALSE;
          // Add the weight values together.
          $weight += $line_item_wrapper->commerce_product->commerce_pm_weight->value();
          // Make the largest of each dimension the total dimension. This is not
          // completely accurate but is as good as it gets unless the order
          // dimensions are manually added in.
          $height = $height > $line_item_wrapper->commerce_product->commerce_pm_height->value() ? $height : $line_item_wrapper->commerce_product->commerce_pm_height->value();
          $length = $length > $line_item_wrapper->commerce_product->commerce_pm_length->value() ? $length : $line_item_wrapper->commerce_product->commerce_pm_length->value();
          $width = $width > $line_item_wrapper->commerce_product->commerce_pm_width->value() ? $width : $line_item_wrapper->commerce_product->commerce_pm_width->value();
          $customs[] = array(
            'description' => $line_item_wrapper->commerce_product->title->value(),
            'country_of_origin' => variable_get('commerce_postmaster_country_origin', t('US')),
            'quantity' => str_replace('.00', '', $line_item_wrapper->quantity->value()) ,
            'value' => strval($line_item_wrapper->commerce_total->amount->value() / 100),
            'weight' => $line_item_wrapper->commerce_product->commerce_pm_weight->value(),
          );
        }
      }
    }

    // Reset the overall dimension values to the order values if they exist.
    if (isset($wrapper->commerce_pm_weight)) {
      $default = FALSE;
      $weight = $wrapper->commerce_pm_weight->value();
      $height = $wrapper->commerce_pm_height->value();
      $length = $wrapper->commerce_pm_length->value();
      $width = $wrapper->commerce_pm_width->value();
    }

    // Default the values to the default variables.
    if ($default) {
      $weight = variable_get('commerce_postmaster_dim_weight', 1);
      $height = variable_get('commerce_postmaster_dim_height', 1);
      $length = variable_get('commerce_postmaster_dim_length', 1);
      $width = variable_get('commerce_postmaster_dim_width', 1);
    }
    if ($weight == 0) {
      $weight = 1;
    }
    if ($height == 0) {
      $height = 1;
    }
    if ($length == 0) {
      $length = 1;
    }
    if ($width == 0) {
      $width = 1;
    }

    // Create the return array.
    $return = array(
      'weight' => $weight,
      'height' => $height,
      'length' => $length,
      'width' => $width,
      'customs' => $customs,
    );

    return $return;
  }

  /**
   * Provides a list of all of the countries in drupal for use in select lists.
   *
   * @return array
   *   An associative array of country names keyed by the country code.
   */
  public static function countryGetList() {
    require_once DRUPAL_ROOT . '/includes/locale.inc';
    return country_get_list();
  }

  /**
   * Returns the Postmaster service level based on the product.
   *
   * Get the default shipping service level for the product if there is only one
   * product, else get the site default.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return string
   *   the service level string required to create an tracking id in postmaster.
   */
  public static function orderSlDefault($order) {
    if (isset($order->commerce_pm_service_level) && isset($order->commerce_pm_service_level[LANGUAGE_NONE])) {
      return $order->commerce_pm_service_level[LANGUAGE_NONE][0]['value'];
    }
    if (isset($order->commerce_line_items[LANGUAGE_NONE]) && count($order->commerce_line_items[LANGUAGE_NONE]) < 3) {
      $line_item = commerce_line_item_load($order->commerce_line_items[LANGUAGE_NONE][0]['line_item_id']);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      if (isset($product->commerce_pm_service_level) && isset($product->commerce_pm_service_level[LANGUAGE_NONE])) {
        return $product->commerce_pm_service_level[LANGUAGE_NONE][0]['value'];
      }
    }
    return variable_get('commerce_postmaster_sl_local', t('GROUND'));
  }

  /**
   * Returns the Postmaster international service level based on the product.
   *
   * Get the default shipping international service level for the product if
   * there is only one product, else get the site default.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return string
   *   The service level string required to create an tracking id in postmaster.
   */
  public static function orderSliDefault($order) {
    if (isset($order->commerce_pm_intl_service_level) && isset($order->commerce_pm_intl_service_level[LANGUAGE_NONE])) {
      return $order->commerce_pm_intl_service_level[LANGUAGE_NONE][0]['value'];
    }
    if (isset($order->commerce_line_items[LANGUAGE_NONE]) && count($order->commerce_line_items[LANGUAGE_NONE]) < 3) {
      $line_item = commerce_line_item_load($order->commerce_line_items[LANGUAGE_NONE][0]['line_item_id']);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      if (isset($product->commerce_pm_intl_service_level) && isset($product->commerce_pm_intl_service_level[LANGUAGE_NONE])) {
        return $product->commerce_pm_intl_service_level[LANGUAGE_NONE][0]['value'];
      }
    }
    return variable_get('commerce_postmaster_sl_intl', t('INTL_SURFACE'));
  }

  /**
   * Returns the Postmaster packaging type based on the product.
   *
   * Get the default shipping packaging for the product if there is only one
   * product, else get the site default.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return string
   *   The package type string required to create an tracking id in postmaster.
   */
  public static function orderPakDefault($order) {
    if (isset($order->commerce_pm_packaging) && isset($order->commerce_pm_packaging[LANGUAGE_NONE])) {
      return $order->commerce_pm_packaging[LANGUAGE_NONE][0]['value'];
    }
    if (isset($order->commerce_line_items[LANGUAGE_NONE]) && count($order->commerce_line_items[LANGUAGE_NONE]) < 3) {
      $line_item = commerce_line_item_load($order->commerce_line_items[LANGUAGE_NONE][0]['line_item_id']);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      if (isset($product->commerce_pm_packaging) && isset($product->commerce_pm_packaging[LANGUAGE_NONE])) {
        return $product->commerce_pm_packaging[LANGUAGE_NONE][0]['value'];
      }
    }
    return variable_get('commerce_postmaster_packaging', t('CARRIER_BOX_SMALL'));
  }

  /**
   * Returns the Postmaster shipping label based on the product.
   *
   * Get the default shipping label format for the product if there is only one
   * product, else get the site default.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return string
   *   The label format string required to create an tracking id in postmaster.
   */
  public static function orderLfDefault($order) {
    if (isset($order->commerce_pm_label_formats) && isset($order->commerce_pm_label_formats[LANGUAGE_NONE])) {
      return $order->commerce_pm_label_formats[LANGUAGE_NONE][0]['value'];
    }
    if (isset($order->commerce_line_items[LANGUAGE_NONE]) && count($order->commerce_line_items[LANGUAGE_NONE]) < 3) {
      $line_item = commerce_line_item_load($order->commerce_line_items[LANGUAGE_NONE][0]['line_item_id']);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      if (isset($product->commerce_pm_label_formats) && isset($product->commerce_pm_label_formats[LANGUAGE_NONE])) {
        return $product->commerce_pm_label_formats[LANGUAGE_NONE][0]['value'];
      }
    }
    return variable_get('commerce_postmaster_label_formats', t('PNG'));
  }

  /**
   * Returns the Postmaster shipping label format based on the product.
   *
   * Get the default shipping label format for the product if there is only one
   * product, else get the site default.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return string
   *   The label size string required to create an tracking id in postmaster.
   */
  public static function orderLsDefault($order) {
    if (isset($order->commerce_pm_label_sizes) && isset($order->commerce_pm_label_sizes[LANGUAGE_NONE])) {
      return $order->commerce_pm_label_sizes[LANGUAGE_NONE][0]['value'];
    }
    if (isset($order->commerce_line_items[LANGUAGE_NONE]) && count($order->commerce_line_items[LANGUAGE_NONE]) < 3) {
      $line_item = commerce_line_item_load($order->commerce_line_items[LANGUAGE_NONE][0]['line_item_id']);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      if (isset($product->commerce_pm_label_sizes) && isset($product->commerce_pm_label_sizes[LANGUAGE_NONE])) {
        return $product->commerce_pm_label_sizes[LANGUAGE_NONE][0]['value'];
      }
    }
    return variable_get('commerce_postmaster_label_sizes', t('SMALL'));
  }

  /**
   * Returns the Postmaster service level based on the order.
   *
   * Get the service level from the order or from the site defaults if that
   * doesn't exist.
   *
   * @param object $order
   *   The current order object.
   * @param string $country
   *   The country for this order.
   *
   * @return string
   *   The label size string required to create an tracking id in postmaster.
   */
  protected static function getServiceLevel($order, $country) {
    // If it's a US order then get the local level.
    if ($country == 'US') {
      if (isset($order->commerce_pm_service_level) && isset($order->commerce_pm_service_level[LANGUAGE_NONE])) {
        return $order->commerce_pm_service_level[LANGUAGE_NONE][0]['value'];
      }
      else {
        return variable_get('commerce_postmaster_sl_local', t('GROUND'));
      }
    }
    // Otherwise use the international level.
    else {
      if (isset($order->commerce_pm_intl_service_level) && isset($order->commerce_pm_intl_service_level[LANGUAGE_NONE])) {
        return $order->commerce_pm_intl_service_level[LANGUAGE_NONE][0]['value'];
      }
      else {
        return variable_get('commerce_postmaster_sl_intl', t('INTL_SURFACE'));
      }
    }
  }

  /**
   * Returns the Postmaster packaging type based on the order.
   *
   * Get the packaging from the order or from the defaults if that doesn't
   * exist.
   *
   * @param object $order
   *   The current order object.
   *
   * @return string
   *   The current default packing setting.
   */
  protected static function getPackaging($order) {
    if (isset($order->commerce_pm_packaging)) {
      $order->commerce_pm_packaging['#access'] = user_access('manage shipping options');
    }
    if (isset($order->commerce_pm_packaging) && isset($order->commerce_pm_packaging[LANGUAGE_NONE])) {
      return $order->commerce_pm_packaging[LANGUAGE_NONE][0]['value'];
    }
    else {
      return variable_get('commerce_postmaster_packaging', t('CARRIER_BOX_SMALL'));
    }
  }

  /**
   * Returns the Postmaster label types based on the order.
   *
   * Get the label formats from the order or from the defaults if that doesn't
   * exist.
   *
   * @param string $formats
   *   The label format short code
   *
   * @return string
   *   The current default packing setting.
   */
  protected static function getLabelTypes($formats) {
    $types = array(
      'EPL' => 'THERMAL',
      'ZPL2' => 'THERMAL',
      'PNG' => 'NORMAL',
      'GIF' => 'NORMAL',
      'PDF' => 'NORMAL',
    );
    return $types[$formats];
  }

  /**
   * Returns the Postmaster label formats based on the order.
   *
   * Get the label formats from the order or from the defaults if that doesn't
   * exist.
   *
   * @param object $order
   *   The current order object.
   *
   * @return string
   *   The current default label type.
   */
  protected static function getLabelFormats($order) {
    if (isset($order->commerce_pm_label_formats)) {
      $order->commerce_pm_label_formats['#access'] = user_access('manage shipping options');
    }
    if (isset($order->commerce_pm_label_formats) && isset($order->commerce_pm_label_formats[LANGUAGE_NONE])) {
      return $order->commerce_pm_label_formats[LANGUAGE_NONE][0]['value'];
    }
    else {
      return variable_get('commerce_postmaster_label_formats', t('PNG'));
    }
  }

  /**
   * Returns the Postmaster service label size on the order.
   *
   * Get the service level from the order or from the defaults if that doesn't
   * exist.
   *
   * @param object $order
   *   The current order object.
   *
   * @return string
   *   The current default label size.
   */
  protected static function getLabelSizes($order) {
    if (isset($order->commerce_pm_label_sizes) && isset($order->commerce_pm_label_sizes[LANGUAGE_NONE])) {
      return $order->commerce_pm_label_sizes[LANGUAGE_NONE][0]['value'];
    }
    else {
      return variable_get('commerce_postmaster_label_sizes', t('SMALL'));
    }
  }

  /**
   * Determines if the current orders shipping country is US.
   *
   * @param object $order
   *   A commerce order.
   *
   * @return bool
   *   True is the orders country is US.
   */
  public static function shippingInternational($order) {
    $profile = commerce_customer_profile_load($order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);
    $country = $profile->commerce_customer_address[LANGUAGE_NONE][0]['country'];
    return $country == 'US';
  }

  /**
   * Sets an error message for not having the API library installed.
   */
  protected static function setApiErrorMesg() {
    drupal_set_message(t('Please install the postmaster API library. Check the !readme for further information.', array('!readme' => l(t('read me'), 'admin/help/commerce_postmaster'))), 'error');
  }
}
